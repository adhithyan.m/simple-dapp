const { expect } = require("chai");
const { ethers } = require("hardhat");
const provider = waffle.provider; //The provider (provided by the waffle package) allows you to read from the contract in tests.

describe("Greeter", function () {
  //test 1
  it("Should return the new greeting once it's changed", async function () {
    /**
     * All this does is, you have a greeting. then you change it, check again if its changed.
     */
    //creating contract object and deploying virtually 
    const Greeter = await ethers.getContractFactory("Greeter");
    const greeter = await Greeter.deploy("Hello, world!"); //deploy it with the greeting of hello world.
    await greeter.deployed();

    expect(await greeter.greet()).to.equal("Hello, world!");     //expecting that greet() should return hello world!

    const setGreetingTx = await greeter.setGreeting("Hola, mundo!");   //now we set it to Hola, mundo! and save it in setGreetingTx

    // wait until the transaction is mined
    await setGreetingTx.wait();

    expect(await greeter.greet()).to.equal("Hola, mundo!");   //expecting that greet() should return Hola, mundo!
  });
  
  //another test 2
  it("Should return the new balance", async function () {
    const Greeter = await ethers.getContractFactory("Greeter");
    const greeter = await Greeter.deploy("Hello, world!"); 
    await greeter.deployed();           // 0 ether in the address

    await greeter.deposit({value: 10});             //we deposited 10
    expect(await provider.getBalance(greeter.address)).to.equal(10);         //we expect 10
  });
});
